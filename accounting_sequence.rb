class Invoice < ActiveRecord::Base
  def self.sequence
    AccountingSequence.where(:country => Settings[:country], "Invoice", Date.today.year).first
  end

  def set_invoice_number
    self.invoice_number = self.class.sequence.next!
  end
end

class AccountingSequence < ActiveRecord::Base
  # AccountingSequence(id:integer, country:string, model:string, year:integer, start_at:integer
  has_many :numbers, :class => "AccountingSequence::Number"
  def next!
    transaction do
      execute("INSERT INTO SQL MAGIC SET NUMBER=NUMBER+1")
      find
    end
  end
end

class AccountingSequence::Number < ActiveRecord::Base
  belongs_to :accounting_sequence
  # AccountingSequence(id:integer, sequence_id:integer, number:integer)
end